//
//  ViewSnapViewController.swift
//  SnapClone
//
//  Created by Christopher Wulle on 9/1/17.
//  Copyright © 2017 Christopher Wulle. All rights reserved.
//

import UIKit
import SDWebImage

class ViewSnapViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var label: UILabel!
    
    var snap = Snap()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.sd_setImage(with: URL(string: snap.imageURL))

        label.text = snap.descrip
    }

}
