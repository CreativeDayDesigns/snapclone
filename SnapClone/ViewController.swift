//
//  ViewController.swift
//  SnapClone
//
//  Created by Christopher Wulle on 8/17/17.
//  Copyright © 2017 Christopher Wulle. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func signinTapped(_ sender: Any) {
        
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!, completion: { (user, error) in
            print("Tried to sign in")
            if error != nil {
                print("Error: \(error)")
                
                Auth.auth().createUser(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!, completion: { (user, error) in
                    print("We tried to create a user")
                    
                    if error != nil {
                        print("Error: \(error)")
                    } else {
                        print("Created User Successfully")
                        Database.database().reference().child("users").child(user!.uid).child("email").setValue(user!.email!)
                        
                        self.performSegue(withIdentifier: "signInSegue", sender: nil)
                    }
                })
                
            } else {
                print("Signed IN!!!")
                self.performSegue(withIdentifier: "signInSegue", sender: nil)
            }
        })
        
    }
    
    
}

